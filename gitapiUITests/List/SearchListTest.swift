//
//  SearchListTest.swift
//  gitapiUITests
//
//  Created by Pooja on 7/14/19.
//  Copyright © 2019 Jaspreet Kumar. All rights reserved.
//

import XCTest
@testable import gitapi

class SearchListTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    func testListFailure() {
        let app = XCUIApplication()
        app.launch()
        
        let projectNameTextField = app.textFields["Project Name (e.g. twitter)"]
        let repoNameTextField = app.textFields["Repo name (e.g. rsc)"]
        let searchButton = app.buttons["Search"]
        
        XCTAssert(projectNameTextField.exists)
        XCTAssert(repoNameTextField.exists)
        XCTAssert(searchButton.exists)
        
        projectNameTextField.tap()
        projectNameTextField.typeText("testgit")
        
        repoNameTextField.tap()
        repoNameTextField.typeText("any invalid repo")
    }
}
